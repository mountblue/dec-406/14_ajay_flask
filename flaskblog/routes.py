import secrets
import os
from PIL import Image
from flaskblog.models import User, Question, Answer
from .handler import get_searched_data
from flask import render_template, url_for, flash, redirect, request, abort, jsonify
from flaskblog.forms import RegistrationForm, LoginForm, UpdateAccountForm, QuestionForm, AnswerForm
from flaskblog import app, celery, db, bcrypt, APP_ROOT
from flask_login import login_user, current_user, logout_user, login_required
import json


@app.route("/") 								#forword slash show the root page for the site
@app.route("/home", methods=['GET', 'POST'])
def home():
	questions= Question.query.all()
	return render_template("home.html", questions = questions)


@app.route("/register", methods=['GET', 'POST'])
def register():
	if current_user.is_authenticated:
		return redirect(url_for('home'))
	form = RegistrationForm()
	if form.validate_on_submit():
		hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
		user = User(username=form.username.data, email=form.email.data, password=hashed_password)
		db.session.add(user)
		db.session.commit()
		flash(f'Your account has been created! You are now able to login', 'success')
		return redirect(url_for('login'))
	return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('home'))
	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data).first()
		if user and bcrypt.check_password_hash(user.password, form.password.data):
			login_user(user, remember=form.remember.data)
			next_page = request.args.get('next')
			return redirect(next_page) if next_page else redirect(url_for('home'))
		else:
			flash(f'Login Unsuccessful. Please check email and password', 'danger')
	return render_template('login.html', title='Login', form=form)


@app.route("/logout")
def logout():
	logout_user()
	return redirect(url_for('login'))	


def save_picture(form_picture):
	random_hex = secrets.token_hex(8)
	_, f_ext = os.path.splitext(form_picture.filename)
	picture_fn = random_hex + f_ext
	picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)
	output_size = (125,125)
	i = Image.open(form_picture)
	i.thumbnail(output_size)
	i.save(picture_path)
	return picture_fn


@app.route("/account", methods=['GET', 'POST'])
@login_required
def account():
	form = UpdateAccountForm()
	if form.validate_on_submit():
		if form.picture.data:
			picture_file = save_picture(form.picture.data)
			current_user.image_file = picture_file
		current_user.username = form.username.data
		current_user.email = form.email.data
		db.session.commit()
		flash('Your account has been updated','success')
		return redirect(url_for('account'))
	elif request.method == 'GET':
		form.username.data = current_user.username
		form.email.data = current_user.email
	image_file = url_for('static', filename='profile_pics/'+current_user.image_file)
	return render_template('account.html', title='Account', image_file = image_file, form = form)

	
@app.route("/question/new", methods=['GET', 'POST'])
@login_required
def new_question():
	form = QuestionForm()
	if form.validate_on_submit():
		question = Question(subject=form.subject.data, content=form.content.data, author=current_user)
		db.session.add(question)
		db.session.commit()
		flash('Your Question has been created', 'success')
		return redirect(url_for('question', question_id=question.id))
	return render_template('create_question.html', title='New Question', form=form, legend='Create Question')


@app.route("/check", methods=['GET', 'POST'])
def search():
	query = request.form.get("query")


@app.route("/question/<int:question_id>", methods=['GET', 'POST'])
def question(question_id):
	question = Question.query.get_or_404(question_id)
	answers = Answer.query.filter_by(question_id=question_id).order_by(Answer.date_posted.desc())
	form = AnswerForm()
	if form.validate_on_submit() and current_user.is_authenticated:
		answer = Answer(content=form.content.data, author=current_user, question_id=question_id )
		db.session.add(answer)
		db.session.commit()
		flash('Your answer has been created', 'success')
		return redirect(url_for('question', question_id=question.id))
	return render_template('question.html', title=question, question=question, answers=answers, form=form) 


@app.route("/question/<int:question_id>/update", methods=['GET', 'POST'])
@login_required
def update_question(question_id):
	question = Question.query.get_or_404(question_id)
	if question.author != current_user:
		abort(403) 
	form = QuestionForm()
	if form.validate_on_submit():
		question.subject = form.subject.data
		question.content = form.content.data
		db.session.commit()
		flash("Your post has been updated", "success")
		return redirect(url_for('question',question_id = question.id))
	elif request.method == 'GET':
		form.subject.data = question.subject
		form.content.data = question.content
	return render_template('create_question.html', title='Update Question', form=form, legend='Update Question')


@app.route("/question/<int:question_id>/delete", methods=['GET', 'POST'])
@login_required
def delete_question(question_id):
	question = Question.query.get_or_404(question_id)
	if question.author != current_user:
		abort(403)
	db.session.delete(question)
	db.session.commit()
	flash("Your post has been deleted", "success")
	return redirect(url_for('home'))


@app.route('/download', methods=['GET', 'POST'])
def download():
	task = download_data.delay(current_user.id)
	return jsonify({}), 202, {'d_status': url_for('download_status',
                                                  task_id=task.id)}


@celery.task(bind=True)
def download_data(self, user_id):
	questions = Question.query.filter_by(user_id=user_id)
	filename = APP_ROOT  + '/static/data.txt'
	i = 1
	with open(filename, 'w') as f:
		f.write("Asked Questions : \n\n")
		for question in questions:
			f.write("\t%s\n" % question.content)
			f.write("\n")
			answers = Answer.query.filter_by(question_id=question.id)
			for answer in answers:
				f.write("\t\t\t%s\n" % answer.content)
				f.write("\n")
			f.write("\n")
		return {
	        'current': 100,
	        'status': 'Task Completed',
	        'filename': 'data.txt'
	    }


@app.route('/status/<task_id>', methods=['GET', 'POST'])
def download_status(task_id):
    task = download_data.AsyncResult(task_id)
    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'current': 0,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'status': task.info.get('status', '')
        }
        if 'filename' in task.info:
            response['filename'] = task.info['filename']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)


@app.route("/home_search", methods=['GET', 'POST'])
def home_serach():
	search = request.form['query']
	if search is not None and search != " ":
		questions= get_searched_data(search)
	else:
		questions= Question.query.all()
	return render_template("home.html", questions = questions)