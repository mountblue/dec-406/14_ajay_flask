from flaskblog.models import User, Question, Answer
from flaskblog.search import add_to_index, remove_from_index, query_index
from flask import jsonify
from flaskblog import celery
from flaskblog import app
import os


def get_searched_data(search_pattern):
    data = []
    questions = Question.query.all()
    for question in questions:
        add_to_index('posts', question)     #add
    questions = query_index('posts', search_pattern, 1, 100)    #query
    db_question = None
    for question in questions:
        temp = {}
        db_question = Question.query.filter_by(content=question['content']).first()
        if db_question is not None:     #searched  question
            temp['author'] = User.query.filter_by(id=db_question.user_id).first()
            temp['id'] = db_question.id
            temp['content'] = question['content']
            temp['date_posted'] = db_question.date_posted
            answers = Answer.query.filter_by(question_id=db_question.id).all()
            temp['answer'] = []            
            for answer in answers:  #its answers
                temp_answer = {}
                temp_answer['content'] = answer.content
                temp_answer['author'] = User.query.filter_by(id=answer.user_id).first().username
                temp['answer'].append(temp_answer)
            data.append(temp)
    return data



